FROM maven:3.5-jdk-8

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

COPY . /usr/src/app
RUN apt-get update && apt-get install entr -y
RUN mvn clean package --batch-mode
ENTRYPOINT java -jar target/ip-config-0.0.1-SNAPSHOT.jar
